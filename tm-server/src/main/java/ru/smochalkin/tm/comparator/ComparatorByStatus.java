package ru.smochalkin.tm.comparator;

import org.jetbrains.annotations.NotNull;
import ru.smochalkin.tm.api.model.IHasStatus;

import java.util.Comparator;

public final class ComparatorByStatus implements Comparator<IHasStatus> {

    @NotNull
    public final static ComparatorByStatus INSTANCE = new ComparatorByStatus();

    @NotNull
    private ComparatorByStatus() {
    }

    @Override
    public int compare(@NotNull IHasStatus o1, @NotNull IHasStatus o2) {
        if (o1.getStatus() == null) return 1;
        if (o2.getStatus() == null) return -1;
        return o1.getStatus().compareTo(o2.getStatus());
    }

}