import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.smochalkin.tm.endpoint.*;
import ru.smochalkin.tm.marker.SoapCategory;

public class TaskEndpointTest {

    @NotNull
    private final SessionEndpoint sessionEndpoint = new SessionEndpointService().getSessionEndpointPort();

    @NotNull
    private final ProjectEndpoint projectEndpoint = new ProjectEndpointService().getProjectEndpointPort();

    @NotNull
    private final TaskEndpoint taskEndpoint = new TaskEndpointService().getTaskEndpointPort();

    @NotNull
    private SessionDto session;

    private int taskCount;

    @Before
    public void init() {
        session = sessionEndpoint.openSession("admin", "100");
        projectEndpoint.createProject(session, "project 1", "lorem");
        taskEndpoint.createTask(session, "task 1", "lorem");
        taskCount = taskEndpoint.findTaskAll(session).size();
    }

    @After
    public void after() {
        taskEndpoint.removeTaskByName(session, "task 1");
        taskEndpoint.removeTaskByName(session, "task 2");
        projectEndpoint.removeProjectByName(session, "project 1");
    }

    @Test
    @Category(SoapCategory.class)
    public void createTaskTest() {
        taskEndpoint.createTask(session, "task 2", "goodbye");
        taskCount++;
        Assert.assertEquals(taskCount, taskEndpoint.findTaskAll(session).size());
    }

    @Test
    @Category(SoapCategory.class)
    public void clearTasksTest() {
        taskEndpoint.clearTasks(session);
        Assert.assertEquals(0, taskEndpoint.findTaskAll(session).size());
    }

    @Test
    @Category(SoapCategory.class)
    public void changeTaskStatusByIdTest() {
        @NotNull final String taskId = taskEndpoint.findTaskByName(session, "task 1").getId();
        @NotNull final Result result = taskEndpoint.changeTaskStatusById(session, taskId, "COMPLETED");
        Assert.assertTrue(result.isSuccess());
    }

    @Test
    @Category(SoapCategory.class)
    public void changeTaskStatusByNameTest() {
        @NotNull final Result result = taskEndpoint
                .changeTaskStatusByName(session, "task 1", "COMPLETED");
        Assert.assertTrue(result.isSuccess());
    }

    @Test
    @Category(SoapCategory.class)
    public void changeTaskStatusByIndexTest() {
        @NotNull final Result result = taskEndpoint.changeTaskStatusByIndex(session, 0, "COMPLETED");
        Assert.assertTrue(result.isSuccess());
    }

    @Test
    @Category(SoapCategory.class)
    public void findTaskAllTest() {
        Assert.assertNotNull(taskEndpoint.findTaskAll(session));
    }

    @Test
    @Category(SoapCategory.class)
    public void findTaskAllSortedTest() {
        Assert.assertNotNull(taskEndpoint.findTaskAllSorted(session, "NAME"));
    }

    @Test
    @Category(SoapCategory.class)
    public void findTaskByIdTest() {
        @NotNull final String taskId = taskEndpoint.findTaskByName(session, "task 1").getId();
        @NotNull final TaskDto task = taskEndpoint.findTaskById(session, taskId);
        Assert.assertEquals("task 1", task.getName());
    }

    @Test
    @Category(SoapCategory.class)
    public void findTaskByNameTest() {
        @NotNull final TaskDto task = taskEndpoint.findTaskByName(session, "task 1");
        Assert.assertEquals("lorem", task.getDescription());
    }

    @Test
    @Category(SoapCategory.class)
    public void findTaskByIndexTest() {
        @NotNull final TaskDto task = taskEndpoint.findTaskByIndex(session, 0);
        Assert.assertEquals("task 1", task.getName());
    }

    @Test
    @Category(SoapCategory.class)
    public void removeTaskByIdTest() {
        @NotNull final String taskId = taskEndpoint.findTaskByName(session, "task 1").getId();
        @NotNull final Result result = taskEndpoint.removeTaskById(session, taskId);
        Assert.assertTrue(result.isSuccess());
    }

    @Test
    @Category(SoapCategory.class)
    public void removeTaskByNameTest() {
        @NotNull final Result result = taskEndpoint.removeTaskByName(session, "task 1");
        Assert.assertTrue(result.isSuccess());
    }

    @Test
    @Category(SoapCategory.class)
    public void removeTaskByIndexTest() {
        @NotNull final Result result = taskEndpoint.removeTaskByIndex(session, 0);
        Assert.assertTrue(result.isSuccess());
    }

    @Test
    @Category(SoapCategory.class)
    public void updateTaskByIdTest() {
        @NotNull final String taskId = taskEndpoint.findTaskByName(session, "task 1").getId();
        taskEndpoint.updateTaskById(session, taskId, "new name", "new desc");
        @NotNull final TaskDto task = taskEndpoint.findTaskById(session, taskId);
        Assert.assertEquals("new name", task.getName());
        Assert.assertEquals("new desc", task.getDescription());
    }

    @Test
    @Category(SoapCategory.class)
    public void updateTaskByIndexTest() {
        taskEndpoint.updateTaskByIndex(session, 0, "new name", "new desc");
        @NotNull final TaskDto task = taskEndpoint.findTaskByIndex(session, 0);
        Assert.assertEquals("new name", task.getName());
        Assert.assertEquals("new desc", task.getDescription());
    }

    @Test
    @Category(SoapCategory.class)
    public void bindTaskByProjectId() {
        @NotNull final String projectId = projectEndpoint.findProjectByName(session, "project 1").getId();
        @NotNull final String taskId = taskEndpoint.findTaskByName(session, "task 1").getId();
        @NotNull final Result result = taskEndpoint.bindTaskByProjectId(session, projectId, taskId);
        Assert.assertTrue(result.isSuccess());
    }

    @Test
    @Category(SoapCategory.class)
    public void unbindTaskByProjectId() {
        @NotNull final String projectId = projectEndpoint.findProjectByName(session, "project 1").getId();
        @NotNull final String taskId = taskEndpoint.findTaskByName(session, "task 1").getId();
        taskEndpoint.bindTaskByProjectId(session, projectId, taskId);
        @NotNull final Result result = taskEndpoint.unbindTaskByProjectId(session, projectId, taskId);
        Assert.assertTrue(result.isSuccess());
    }

    @Test
    @Category(SoapCategory.class)
    public void findTasksByProjectIdTest() {
        @NotNull final String projectId = projectEndpoint.findProjectByName(session, "project 1").getId();
        @NotNull final String taskId = taskEndpoint.findTaskByName(session, "task 1").getId();
        taskEndpoint.bindTaskByProjectId(session, projectId, taskId);
        Assert.assertEquals(1, taskEndpoint.findTasksByProjectId(session, projectId).size());
    }

}
