package ru.smochalkin.tm.listener;

import org.jetbrains.annotations.Nullable;
import ru.smochalkin.tm.endpoint.ProjectDto;
import ru.smochalkin.tm.exception.entity.ProjectNotFoundException;

public abstract class AbstractProjectListener extends AbstractListener {

    protected void showProject(@Nullable final ProjectDto project) {
        if (project == null) throw new ProjectNotFoundException();
        System.out.println("Id: " + project.getId());
        System.out.println("Name: " + project.getName());
        System.out.println("Description: " + project.getDescription());
        System.out.println("Status: " + project.getStatus());
    }

}
